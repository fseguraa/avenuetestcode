package nproject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class task2 {


    public static void main(String[] args) {
       
    	System.setProperty("we-bdriver.firefox.marionette","C:\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		
    	
        String baseUrl = "https://qa-test.avenuecode.com/tasks";
 
       
        driver.get(baseUrl);
		
		WebElement managebutton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/table/tbody/tr/td[4]/button");
		driver.click(managebutton)
		
		String expectedTitle = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/h3")).getText();
		if ( expectedTitle.matches("Editing Task(.*)") == true ) {
			
			System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
		
		
		WebElement subfield= driver.findElement(By.Id("new_sub_task").sendkeys("subtask 1")
		WebElement subdate= driver.findElement(By.Id("dueDate").sendkeys("02/10/18")
		WebElement sbutton= driver.findElement(By.Id("add-subtask");
		driver.click(sbutton);
		String expectedlabel = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/a")).getText();
		
		
		if ( expectedlabel.matches("empty") == true || expectedlabel.matches("empty") == null ) {
			
			System.out.println("Test Failed, mandatory field was empty");
        } else {
            System.out.println("Test Passed!");
        }
		
		driver.close();
	}